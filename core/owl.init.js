$(document).on('ready', function(){

	var history = $('.history'),
		brands = $('.brand-slider'),
		testimoni = $('.slider-testimoni'),
		tempObject = [];

	history.on('initialize.owl.carousel', function(e){
		$.each($(this).find('.item'), function (i) {
	        var titleData = $(this).find('span.year').text(),
		        // el = $("<li><a href='javascript:void(0)'>" + titleData + "</a></li>");
	            el = $("<div class='year-page' style='width:70px'><a href='javascript:void(0)'>" + titleData + "</a></div>");

	        tempObject.push(el);

	        el.bind('click', function(e){
	        	history.trigger('to.owl.carousel', [i, 300, true])
	        });

	        $(".history-page > .page-history").append(el);
	    });
	});

	history.on('initialized.owl.carousel', function(e){
		// var prev = $('<li><a class="prev"><i class="fa fa-chevron-left"></i></a></li>'),
		// 	next = $('<li><a class="next"><i class="fa fa-chevron-right"></i></a></li>');
		var prev = $('.history-page a.prev'),
			next = $('.history-page a.next');
		tempObject[e.item.index].addClass('active');

		$('.page-history').owlCarousel({
		    touchDrag: false,
		    items:8
		});

		next.on('click', function(e){
			e.preventDefault();
		    $('.page-history').trigger('next.owl.carousel');
		});
		prev.on('click', function(e){
			e.preventDefault();
		    $('.page-history').trigger('prev.owl.carousel', [300]);
		});

	});

	history.on('changed.owl.carousel', function(e) {
		if(tempObject.length > 0)
		{
			$.each(tempObject, function () {
	            this.removeClass('active');
	        });
	        tempObject[e.item.index].addClass('active');
		}
    });

    history.owlCarousel({
    	items:1,
	 	dots: false
	});

	brands.on('initialize.owl.carousel', function(e){
		$.each($(this).find('.item'), function (i) {
	        var titleData = $(this).find('img').data('logo'),
	            el = $("<li><img src='" + titleData + "'/></li>");
	            
	        tempObject.push(el);

	        el.bind('click', function(e){
	        	brands.trigger('to.owl.carousel', [i, 300, true])
	        });

	        $(".brand-pagination > ul").append(el);
	    });
	});

	brands.on('initialized.owl.carousel', function(e){
		var prev = $('<li><a class="prev"><i class="fa fa-chevron-left"></i></a></li>'),
			next = $('<li><a class="next"><i class="fa fa-chevron-right"></i></a></li>');
		tempObject[e.item.index].addClass('active');

		next.bind('click', function(){
		    brands.trigger('next.owl.carousel');
		});
		prev.bind('click', function(){
		    brands.trigger('prev.owl.carousel', [300]);
		});

		$(".brand-pagination > ul").prepend(prev);
		$(".brand-pagination > ul").append(next);
	});

	brands.on('changed.owl.carousel', function(e) {
		if(tempObject.length > 0)
		{
			$.each(tempObject, function () {
	            this.removeClass('active');
	        });
	        tempObject[e.item.index].addClass('active');
		}
    });

    brands.owlCarousel({
    	items:1,
	 	dots: false
	});

	if(isMobile())
	{
		$('.mobile-slide').owlCarousel({
	 		items:1,
	 		stagePadding: 20,
	 		margin: 30,
	 		dots: false,
	 		nav: true,
			navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
	 	}).removeClass('row');
	}

	$('.vision-slider').owlCarousel({
		animateOut: 'fadeOut',
		items:1,
		// autoplay: true,
		autoplayTimeout: 5000,
		loop: true,
		mouseDrag: false
	});

	$('.brands-slider').on('initialized.owl.carousel', function(e){
		$('.brands-slider .item > a').css({
			height: $('.brands-slider .owl-item.active').innerWidth(),
			width: $('.brands-slider .owl-item.active').innerWidth()
		});
	});

	$('.brands-slider').owlCarousel({
		items:(isMobile() ? 1 : 4),
		stagePadding: 20,
		margin: 30,
		dots: false,
		nav: true,
		navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
	});

	testimoni.owlCarousel({
		items: 1,
		dots: false,
		nav: true,
		navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
	});
});