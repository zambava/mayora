function initMap(data) {
	// init map
    $("#world-map").vectorMap({
        map: 'world_mill_en',
        normalizeFunction: 'polynomial',
        backgroundColor: '#F6F6F6',
        regionStyle: {
            initial: {
                'fill': '#C1C1C1',
                "fill-opacity": 1,
                'stroke': '#C1C1C1',
                "stroke-width": 1,
                "stroke-opacity": 1
            },
            hover: {
                "fill" : "#F4071B"
            }
        },
        markerStyle : {
            initial: {
                fill: '#FFF',
                r: 4,
                'stroke-width': 2
            },
            hover: {
                cursor: 'pointer'
            }
        },
        markerTipStyle: {
            initial: {
                cursor: 'default',
                fill: '#FFF'
              },
              hover: {
                cursor: 'pointer'
              }
        },
        onMarkerLabelShow: function(e, el, idx){
            var txt = '<strong class="info">' + el.html() + '</strong>' + 
                        '\n<p>' + data[idx].address + '</p>' +
                        '\n<p>Phone: ' + (data[idx].phone ? data[idx].phone :  '-') + '</p>' + 
                        '\n<p>Fax: ' + (data[idx].fax ? data[idx].fax :  '-') + '</p>';
            
            el.html(txt);
        },
        onRegionLabelShow: function(e, el, code){
            e.preventDefault();
        },
        onRegionClick:function (event, code){
            // $("#world-map").vectorMap('set', 'focus', code);
        },
        markers: data
    });

    var mapObject = $("#world-map").vectorMap('get', 'mapObject');

        mapObject.container.click(function(e){
            var latLng = mapObject.pointToLatLng(e.offsetX, e.offsetY);

            // console.clear();
            console.log(latLng.lat.toFixed(2), latLng.lng.toFixed(2));
        });
}

$(document).on('ready', function(){
	var dataMap = $.getJSON('/content/custom/data_dummy/internation-relations.json').
	done(function(data){
        if($("#world-map").length > 0) initMap(data.countries);
	});
});