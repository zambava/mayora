$(document).on('ready', function(we){
    $("a.btn-danger.btn-block input[type='file']").on("change", function(){
      var val = $(this).val(),
          index = val.lastIndexOf('\\') + 1,
          filename = val.substr(index);
      
      $("input#upload-file-info").val(filename);
    });

    // social share handler 
    $('ul.social-share >li > a').on('click', function(e){
        e.preventDefault();

        var winTop = (screen.height / 2) - (350 / 2),
            winLeft = (screen.width / 2) - (520 / 2),
            urlShare = $(this).attr('href');

        window.open(urlShare, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + 520 + ',height=' + 350);
    });

	// fit-screen mode
	if($('.content-wrapper.vision').length > 0)
	{
        setFitScreen();
		$('.vision-slider .item').css({
			height: $('#header .content-wrapper').innerHeight()
		});
	}

	// navbar fit
	$('.subnav').css({
		width: $('.menu-panel').innerWidth() + 'px'
	});

	// matching column height
	$('[data-match-columns="true"]').each(function(){
      if(isMobile()) return;
       
      var $row = $(this), $columns = getColumns($row);
      bindColumns($columns);
      $columns.trigger('resizeHeights');
    });

	// search handler
	$('a.submit').on('click', function(){
		$('form.search-form').submit();
	})

	$('form.search-form').on('submit', function(e){
		var this = $(this);

		if (this.query.value.length < 1) e.preventDefault();
		else return
	})

	// animated element
	$(".animated").appear(function() {
        var element = $(this),
            animation = element.data("animate"),
            animationDelay = element.data("delay");

        if (animationDelay && !isMobile()) {
            setTimeout(function() {
                element.addClass(animation + " visible");
                if (element.hasClass("counter")) {
                    element.find('.value').countTo();
                }
            }, animationDelay);
        } else {
            element.addClass(animation + " visible");
            if (element.hasClass("counter")) {
                element.find(".value").countTo();
            }
        }
    }, {
        accY: -150
    });

    //Collapse CSR image show
	$('#csrscroll').on('show.bs.collapse', function () {
	    $(".caption-center").removeClass('layer-dark');	   
	})

    //Collapse CSR image hidden
	$('#csrscroll').on('hide.bs.collapse', function () {
	    $(".caption-center").addClass('layer-dark');	   
	})

    $('.item-list-panel.row .brand-item > a').css({
        width: $('.item-list-panel.row .brand-item').innerWidth(),
        height: $('.item-list-panel.row .brand-item').innerWidth()
    });

    // form datepicker
    $('.date-picker').datepicker({
        format: 'dd/mm/yyyy'
    });

    if(!isMobile())
    {
        $(".dropdown-menu-large.about li").css({
            width: (100/5) + "%" 
        });
    }
});