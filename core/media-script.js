$(document).on('ready', function(){
  var windowHash = window.location.href.split("#")[1];

	$('.media.news a.view').on('click', function(e){
    e.preventDefault();

    var dataURL = $(this).data('url'),
        dataDesc = $(this).data('desc'),
        date = $(this).parents('.media.news').find('p.date').text(),
        title = $(this).parents('.media.news').find('h3.title').text(),
        type = $(this).data('type'),
        downloadBtn = $('.download-panel').find('button.btn.download'),
        mediaPanel = $('.download-panel .media-list'),
        mediaDate = $('.download-panel .panel-heading p.date'),
        mediaTitle = $('.download-panel .panel-heading h3.title'),
        form = $("form#download_media");

    $('.download-panel').collapse('hide');
    mediaPanel.empty();

    if(type != 'youtube' && type != 'video') {
      var actionURL = '/umbraco/surface/Content/DowloadMedia/' + dataURL.split('/').pop();

      form.attr('action', actionURL);

      getMediaData(dataURL).
      
      success(function (data) {
            mediaDate.text(date);
            mediaTitle.text(title);
            mediaPanel.append(data);
            
            setTimeout(function(){
              downloadBtn.show();
              $('.download-panel').collapse('show');
            }, 1000);
          }).

        error(function (data) {
          console.error('getting data error');
        });
    }

    else {
      var VideoUrl = (type == 'youtube') ? '<div class="col-sm-6"><iframe style="width:100%;height:295px" src="https://www.youtube.com/embed/' + dataURL + '" frameborder="0" allowfullscreen></iframe></div>' : '<div class="col-sm-6"><iframe style="width:100%;height:295px" src="' + dataURL + '" frameborder="0" allowfullscreen></iframe></div>',
          videoContent = $(VideoUrl),
          videoDesc = $('<div class="col-sm-6"><p class="video-desc">' + dataDesc + '</p></div>');

      mediaPanel.append(videoContent).append(videoDesc);
      mediaDate.text(date);
      mediaTitle.text(title);

      setTimeout(function(){
        downloadBtn.hide();
        $('.download-panel').collapse('show');
      }, 1000);
    }
	});

	$('.download-panel button.close').on('click', function(){
    $('.download-panel').collapse('hide');
  });

  $('.download-panel').on('shown.bs.collapse', function () {
    $.smoothScroll({
      offset: -30,
      scrollTarget: '.download-panel',
      speed: 500
    });
    $('.media-list .media-item > a').css({
        display: 'block',
        height: $('.media-list .media-item').innerWidth() - 30
    });
    $('.media-list .media-item').css({
        height: $('.media-list .media-item').innerWidth() - 30
    }).
    animate({
    	opacity: 1
    }, 1000);
  
  });

  //anchor media handle
  if (windowHash) {
   $("#" + windowHash + ".media.news a.view").click();
  }
});