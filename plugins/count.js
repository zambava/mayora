! function(e) {
    e.fn.countTo = function(h) {
        return h = h || {}, e(this).each(function() {
            function b(a) {
                a = d.formatter.call(n, a, d);
                k.text(a)
            }
            var d = e.extend({}, e.fn.countTo.defaults, {
                from: e(this).data("from"),
                to: e(this).data("to"),
                speed: e(this).data("speed"),
                refreshInterval: e(this).data("refresh-interval"),
                decimals: e(this).data("decimals")
            }, h),
                a = Math.ceil(d.speed / d.refreshInterval),
                c = (d.to - d.from) / a,
                n = this,
                k = e(this),
                p = 0,
                m = d.from,
                f = k.data("countTo") || {};
            k.data("countTo", f);
            f.interval && clearInterval(f.interval);
            f.interval = setInterval(function() {
                m += c;
                p++;
                b(m);
                "function" == typeof d.onUpdate && d.onUpdate.call(n, m);
                p >= a && (k.removeData("countTo"), clearInterval(f.interval), m = d.to, "function" == typeof d.onComplete && d.onComplete.call(n, m))
            }, d.refreshInterval);
            b(m)
        })
    };
    e.fn.countTo.defaults = {
        from: 0,
        to: 0,
        speed: 1E3,
        refreshInterval: 100,
        decimals: 0,
        formatter: function(e, b) {
            return e.toFixed(b.decimals)
        },
        onUpdate: null,
        onComplete: null
    }
}(jQuery);