! function(e) {
    "function" == typeof define && define.amd ? define(["jquery"], e) : e(jQuery)
}(function(e) {
    function h(a) {
        return a.replace(/(:|\.|\/)/g, "\\$1")
    }
    var b = {}, d = function(a) {
        var b = [],
            d = !1,
            h = a.dir && "left" === a.dir ? "scrollLeft" : "scrollTop";
        return this.each(function() {
            if (this !== document && this !== window) {
                var a = e(this);
                0 < a[h]() ? b.push(this) : (a[h](1), d = 0 < a[h](), d && b.push(this), a[h](0))
            }
        }), b.length || this.each(function() {
            "BODY" === this.nodeName && (b = [this])
        }), "first" === a.el && 1 < b.length && (b = [b[0]]), b
    };
    e.fn.extend({
        scrollable: function(a) {
            a = d.call(this, {
                dir: a
            });
            return this.pushStack(a)
        },
        firstScrollable: function(a) {
            a = d.call(this, {
                el: "first",
                dir: a
            });
            return this.pushStack(a)
        },
        smoothScroll: function(a, b) {
            if (a = a || {}, "options" === a) return b ? this.each(function() {
                var a = e(this),
                    a = e.extend(a.data("ssOpts") || {}, b);
                e(this).data("ssOpts", a)
            }) : this.first().data("ssOpts");
            var d = e.extend({}, e.fn.smoothScroll.defaults, a),
                k = e.smoothScroll.filterPath(location.pathname);
            return this.unbind("click.smoothscroll").bind("click.smoothscroll", function(a) {
                var b = e(this),
                    c = e.extend({}, d, b.data("ssOpts") || {}),
                    q = d.exclude,
                    u = c.excludeWithin,
                    H = 0,
                    B = 0,
                    E = !0,
                    r = {}, t = location.hostname === this.hostname || !this.hostname,
                    L = c.scrollTarget || e.smoothScroll.filterPath(this.pathname) === k,
                    v = h(this.hash);
                if (c.scrollTarget || t && L && v) {
                    for (; E && H < q.length;) b.is(h(q[H++])) && (E = !1);
                    for (; E && B < u.length;) b.closest(u[B++]).length && (E = !1)
                } else E = !1;
                E && (c.preventDefault && a.preventDefault(), e.extend(r, c, {
                    scrollTarget: c.scrollTarget || v,
                    link: this
                }), e.smoothScroll(r))
            }), this
        }
    });
    e.smoothScroll = function(a, c) {
        if ("options" === a && "object" == typeof c) return e.extend(b, c);
        var d, h, p, m, f;
        m = 0;
        var q = "offset",
            u = "scrollTop",
            H = {};
        p = {};
        "number" == typeof a ? (d = e.extend({
            link: null
        }, e.fn.smoothScroll.defaults, b), p = a) : (d = e.extend({
            link: null
        }, e.fn.smoothScroll.defaults, a || {}, b), d.scrollElement && (q = "position", "static" === d.scrollElement.css("position") && d.scrollElement.css("position", "relative")));
        u = "left" === d.direction ? "scrollLeft" : u;
        d.scrollElement ? (h = d.scrollElement, /^(?:HTML|BODY)$/.test(h[0].nodeName) || (m = h[u]())) : h = e("html, body").firstScrollable(d.direction);
        d.beforeScroll.call(h, d);
        p = "number" == typeof a ? a : c || e(d.scrollTarget)[q]() && e(d.scrollTarget)[q]()[d.direction] || 0;
        H[u] = p + m + d.offset;
        m = d.speed;
        "auto" === m && (f = H[u] - h.scrollTop(), 0 > f && (f *= -1), m = f / d.autoCoefficient);
        p = {
            duration: m,
            easing: d.easing,
            complete: function() {
                d.afterScroll.call(d.link, d)
            }
        };
        d.step && (p.step = d.step);
        h.length ? h.stop().animate(H, p) : d.afterScroll.call(d.link, d)
    };
    e.smoothScroll.version = "1.5.4";
    e.smoothScroll.filterPath = function(a) {
        return a = a || "", a.replace(/^\//, "").replace(/(?:index|default).[a-zA-Z]{3,4}$/, "").replace(/\/$/, "")
    };
    e.fn.smoothScroll.defaults = {
        exclude: [],
        excludeWithin: [],
        offset: 0,
        direction: "top",
        scrollElement: null,
        scrollTarget: null,
        beforeScroll: function() {},
        afterScroll: function() {},
        easing: "swing",
        speed: 400,
        autoCoefficient: 2,
        preventDefault: !0
    }
});