(function(e) {
    e.fn.appear = function(h, b) {
        var d = e.extend({
            data: void 0,
            one: !0,
            accX: 0,
            accY: 0
        }, b);
        return this.each(function() {
            var a = e(this);
            if (a.appeared = !1, !h) return void a.trigger("appear", d.data);
            var b = e(window),
                n = function() {
                    if (!a.is(":visible")) return void(a.appeared = !1);
                    var n = b.scrollLeft(),
                        m = b.scrollTop(),
                        f = a.offset(),
                        k = f.left,
                        f = f.top,
                        h = d.accX,
                        e = d.accY,
                        B = a.height(),
                        E = b.height(),
                        r = a.width(),
                        t = b.width();
                    f + B + e >= m && m + E + e >= f && k + r + h >= n && n + t + h >= k ? a.appeared || a.trigger("appear", d.data) : a.appeared = !1
                }, k = function() {
                    if (a.appeared = !0, d.one) {
                        b.unbind("scroll", n);
                        var p = e.inArray(n, e.fn.appear.checks);
                        0 <= p && e.fn.appear.checks.splice(p, 1)
                    }
                    h.apply(this, arguments)
                };
            d.one ? a.one("appear", d.data, k) : a.bind("appear", d.data, k);
            b.scroll(n);
            e.fn.appear.checks.push(n);
            n()
        })
    };
    e.extend(e.fn.appear, {
        checks: [],
        timeout: null,
        checkAll: function() {
            var h = e.fn.appear.checks.length;
            if (0 < h) for (; h--;) e.fn.appear.checks[h]()
        },
        run: function() {
            e.fn.appear.timeout && clearTimeout(e.fn.appear.timeout);
            e.fn.appear.timeout = setTimeout(e.fn.appear.checkAll, 20)
        }
    });
    e.each("append prepend after before attr removeAttr addClass removeClass toggleClass remove css show hide".split(" "), function(h, b) {
        var d = e.fn[b];
        d && (e.fn[b] = function() {
            var a = d.apply(this, arguments);
            return e.fn.appear.run(), a
        })
    })
})(jQuery);