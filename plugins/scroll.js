! function() {
    function e() {
        if (document.body) {
            var a = document.body,
                b = document.documentElement,
                c = window.innerHeight,
                e = a.scrollHeight;
            t = 0 <= document.compatMode.indexOf("CSS") ? b : a;
            f = a;
            u.keyboardSupport && window.addEventListener("keydown", d, !1);
            if (r = !0, top != self) B = !0;
            else if (e > c && (a.offsetHeight <= c || b.offsetHeight <= c)) {
                var h = !1;
                if (b.style.height = "auto", setTimeout(function() {
                    h || b.scrollHeight == document.height || (h = !0, setTimeout(function() {
                        b.style.height = document.height + "px";
                        h = !1
                    }, 500))
                }, 10), t.offsetHeight <= c) c = document.createElement("div"), c.style.clear = "both", a.appendChild(c)
            }
            u.fixedBackground || H || (a.style.backgroundAttachment = "scroll", b.style.backgroundAttachment = "scroll")
        }
    }
    function h(a, b, c, d) {
        if (d || (d = 1E3), k(b, c), 1 != u.accelerationMax) {
            var e = +new Date - w;
            e < u.accelerationDelta && (e = (1 + 30 / e) / 2, 1 < e && (e = Math.min(e, u.accelerationMax), b *= e, c *= e));
            w = +new Date
        }
        if (K.push({
            x: b,
            y: c,
            lastX: 0 > b ? .99 : -.99,
            lastY: 0 > c ? .99 : -.99,
            start: +new Date
        }), !I) {
            var f = a === document.body,
                h = function() {
                    for (var e = +new Date, k = 0, n = 0, p = 0; p < K.length; p++) {
                        var q = K[p],
                            r = e - q.start,
                            t = r >= u.animationTime,
                            v = t ? 1 : r / u.animationTime;
                        u.pulseAlgorithm && (r = v, v = 1 <= r ? 1 : 0 >= r ? 0 : (1 == u.pulseNormalize && (u.pulseNormalize /= m(1)), m(r)));
                        r = q.x * v - q.lastX >> 0;
                        v = q.y * v - q.lastY >> 0;
                        k += r;
                        n += v;
                        q.lastX += r;
                        q.lastY += v;
                        t && (K.splice(p, 1), p--)
                    }
                    f ? window.scrollBy(k, n) : (k && (a.scrollLeft += k), n && (a.scrollTop += n));
                    b || c || (K = []);
                    K.length ? N(h, a, d / u.frameRate + 1) : I = !1
                };
            N(h, a, 0);
            I = !0
        }
    }
    function b(a) {
        r || e();
        var b = a.target,
            c = n(b);
        if (!c || a.defaultPrevented || "embed" === (f.nodeName || "").toLowerCase() || "embed" === (b.nodeName || "").toLowerCase() && /\.pdf/i.test(b.src)) return !0;
        var b = a.wheelDeltaX || 0,
            d = a.wheelDeltaY || 0;
        b || d || (d = a.wheelDelta || 0);
        var k;
        if (k = !u.touchpadSupport) if (k = d) {
            k = Math.abs(k);
            L.push(k);
            L.shift();
            clearTimeout(C);
            k = L[0] == L[1] && L[1] == L[2];
            var m = p(L[0], 120) && p(L[1], 120) && p(L[2], 120);
            k = !(k || m)
        } else k = void 0;
        return k ? !0 : (1.2 < Math.abs(b) && (b *= u.stepSize / 120), 1.2 < Math.abs(d) && (d *= u.stepSize / 120), h(c, - b, - d), a.preventDefault(), void 0)
    }
    function d(a) {
        var b = a.target,
            c = a.ctrlKey || a.altKey || a.metaKey || a.shiftKey && a.keyCode !== v.spacebar;
        if (/input|textarea|select|embed/i.test(b.nodeName) || b.isContentEditable || a.defaultPrevented || c || "button" === (b.nodeName || "").toLowerCase() && a.keyCode === v.spacebar) return !0;
        var d;
        d = b = 0;
        var c = n(f),
            e = c.clientHeight;
        switch (c == document.body && (e = window.innerHeight), a.keyCode) {
            case v.up:
                d = -u.arrowScroll;
                break;
            case v.down:
                d = u.arrowScroll;
                break;
            case v.spacebar:
                d = a.shiftKey ? 1 : -1;
                d = .9 * -d * e;
                break;
            case v.pageup:
                d = .9 * -e;
                break;
            case v.pagedown:
                d = .9 * e;
                break;
            case v.home:
                d = -c.scrollTop;
                break;
            case v.end:
                e = c.scrollHeight - c.scrollTop - e;
                d = 0 < e ? e + 10 : 0;
                break;
            case v.left:
                b = -u.arrowScroll;
                break;
            case v.right:
                b = u.arrowScroll;
                break;
            default:
                return !0
        }
        h(c, b, d);
        a.preventDefault()
    }
    function a(a) {
        f = a.target
    }
    function c(a, b) {
        for (var c = a.length; c--;) T[X(a[c])] = b;
        return b
    }
    function n(a) {
        var b = [],
            d = t.scrollHeight;
        do {
            var e = T[X(a)];
            if (e) return c(b, e);
            if (b.push(a), d === a.scrollHeight) {
                if (!B || t.clientHeight + 10 < d) return c(b, document.body)
            } else if (a.clientHeight + 10 < a.scrollHeight && (overflow = getComputedStyle(a, "").getPropertyValue("overflow-y"), "scroll" === overflow || "auto" === overflow)) return c(b, a)
        } while (a = a.parentNode)
    }
    function k(a, b) {
        a = 0 < a ? 1 : -1;
        b = 0 < b ? 1 : -1;
        (E.x !== a || E.y !== b) && (E.x = a, E.y = b, K = [], w = 0)
    }
    function p(a, b) {
        return Math.floor(a / b) == a / b
    }
    function m(a) {
        var b, c, d;
        return a *= u.pulseScale, 1 > a ? b = a - (1 - Math.exp(-a)) : (c = Math.exp(-1), --a, d = 1 - Math.exp(-a), b = c + d * (1 - c)), b * u.pulseNormalize
    }
    var f, q = {
        frameRate: 150,
        animationTime: 800,
        stepSize: 120,
        pulseAlgorithm: !0,
        pulseScale: 8,
        pulseNormalize: 1,
        accelerationDelta: 20,
        accelerationMax: 1,
        keyboardSupport: !0,
        arrowScroll: 50,
        touchpadSupport: !0,
        fixedBackground: !0,
        excluded: ""
    }, u = q,
        H = !1,
        B = !1,
        E = {
            x: 0,
            y: 0
        }, r = !1,
        t = document.documentElement,
        L = [120, 120, 120],
        v = {
            left: 37,
            up: 38,
            right: 39,
            down: 40,
            spacebar: 32,
            pageup: 33,
            pagedown: 34,
            end: 35,
            home: 36
        }, u = q,
        K = [],
        I = !1,
        w = +new Date,
        T = {};
    setInterval(function() {
        T = {}
    }, 1E4);
    var C, X = function() {
        var a = 0;
        return function(b) {
            return b.uniqueID || (b.uniqueID = a++)
        }
    }(),
        N = function() {
            return window.requestAnimationFrame || window.webkitRequestAnimationFrame || function(a, b, c) {
                window.setTimeout(a, c || 1E3 / 60)
            }
        }(),
        q = /chrome/i.test(window.navigator.userAgent);
    "onmousewheel" in document && q && (window.addEventListener("mousedown", a, !1), window.addEventListener("mousewheel", b, !1), window.addEventListener("load", e, !1))
}();